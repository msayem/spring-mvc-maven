Spring MVC with Maven
=====================
This is a Java web application using Spring MVC Framework with Apache Maven.


Project Environment:
====================
1. Java JDK 1.8

2. Apache Maven 3.3.1

3. Spring Framework 4.1.6

4. Apache Tomcat 8

5. Eclipse Luna 4.4

6. Log4j 1.2.17

7. SLF4J 1.7.12

8. JUnit 4.12

9. Mockito 1.10.19
