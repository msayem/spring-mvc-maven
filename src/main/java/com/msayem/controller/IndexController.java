package com.msayem.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Handles requests for the application index page.
 * 
 * @author MSAYEM
 * 
 */
@Controller
public class IndexController {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String indexController() {
		
		logger.info("spring-mvc-maven: Loading index.jsp page...");
		
		return "index";
	}
}